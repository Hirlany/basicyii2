<?php
namespace app\components;


use app\models\entities\Appskeleton;
use app\models\entities\Appskeletonhasauthitems;
use app\models\entities\Authitems;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;


class BaseController extends Controller
{
    public function behaviors()
    {
//        VarDumper::dump($this->createRules(),10,true);exit;
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $this->createRules(),
            ]
        ];
    }
    public function createRules()
    {
        $menu = Appskeleton::find()
            ->innerJoin('app_skeleton_has_authitems','app_skeleton_has_authitems.idapp_skeleton = app_skeleton.idapp_skeleton')
            ->where('lower(app_skeleton.controllerid)=:controllerid',['controllerid'=>strtolower(\Yii::$app->controller->id)])
            ->all();

        $auth = [];
        $allAuth = [];
        $final = [];



        foreach ($menu as $item)
        {
            foreach ($item->appSkeletonHasAuthitems as $authname)
            {
//                $authname = new Appskeletonhasauthitems();
                if (isset($authname) and !empty($authname)) {
                    $auth[$authname->auth_name]['allow'] = true;
                    $auth[$authname->auth_name]['actions'][] = $item->actionid;
                    if (in_array($authname->auth_name, array_keys(\Yii::$app->params['ProfileExclude'])))
                    {
                        if(!\Yii::$app->user->isGuest and $authname->auth_name=='Publico')
                            $authname->auth_name='Logueado';

                        $auth[$authname->auth_name]['roles'][] = \Yii::$app->params['ProfileExclude'][$authname->auth_name];
                    } else {
                        $auth[$authname->auth_name]['roles'][] = $authname->auth_name;
                    }
                }
            }
        }
        $auth=array_values($auth);

        foreach ($auth as $value)
        {
            foreach ($value as $key=>$item)
            {
                if(is_array($item))
                    $final[$key]=array_unique($item);
                else
                    $final[$key]=$item;
            }
            $allAuth[]=$final;
        }
        return $allAuth;
    }
}